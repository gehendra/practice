ngRaffle = angular.module("raffleModule", ["ngResource"])

ngRaffle.factory "Entry", ["$resource", ($resource) -> 
  $resource("/entries/:id", {id: "@id"}, {update: {method: "PUT"}})
]

ngRaffle.controller("RafflerController", ["$scope", "Entry", ($scope, Entry) ->
  $scope.entries = Entry.query()

  $scope.addEntry = ->
    if ($scope.newEntry && $scope.newEntry.name != null)
      entry = Entry.save($scope.newEntry)
      $scope.entries.push($scope.newEntry)
      $scope.newEntry = {}

  $scope.drawWinner = -> 
    pool = []
    angular.forEach $scope.entries, (entry) -> 
      pool.push(entry) if !entry.winner

    if (pool.length > 0)
      entry = pool[Math.floor(Math.random() * pool.length)]
      entry.winner = true;
      entry.$update()
      $scope.lastWinner = entry
    else
      $scope.allWinner = true
])